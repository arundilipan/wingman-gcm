(defproject wingman-gcm "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :min-lein-version "2.0.0"
  :dependencies [[org.clojure/clojure "1.7.0"]
                 [http-kit "2.1.18"]
                 [com.taoensso/carmine "2.12.2"]
                 [cheshire "5.6.1"]
                 [ring/ring-json "0.4.0"]
                 [compojure "1.4.0"]
                 [clj-http "2.2.0"]
                 [ring/ring-defaults "0.1.5"]]
  :plugins [[lein-ring "0.9.7"]]
  :ring {:handler wingman-gcm.handler/app}
  :aot :all
  :main wingman-gcm.handler
  :profiles
  {:dev {:dependencies [[javax.servlet/servlet-api "2.5"]
                        [ring/ring-mock "0.3.0"]]}})
