(ns wingman-gcm.handler
  (:gen-class)
  (:require [compojure.core :refer :all]
            [org.httpkit.server :refer [run-server]]
            [compojure.route :as route]
            [wingman-gcm.gcm :as gcm]
            [clojure.walk :refer [keywordize-keys]]
            [ring.util.response :refer [response]]
            [ring.middleware.json :refer [wrap-json-body]]
            [ring.middleware.defaults :refer [wrap-defaults api-defaults]]))

(defroutes app-routes
  (POST "/req" {body :body}
    (prn (keywordize-keys body))
    (gcm/send-req (keywordize-keys body))
    (response "Yay!"))

  (POST "/okay" {body :body}
    (gcm/send-okay (keywordize-keys body))
    (response "Yay!"))

  (POST "/send-pushid" {body :body}
    (prn body)
    (gcm/set-push-id (keywordize-keys body)))

  (route/not-found "Not Found"))

(def app
  (-> app-routes
      (wrap-json-body)
      (wrap-defaults api-defaults)))

(defn -main []
  (run-server app {:port 4500}))
