(ns wingman-gcm.gcm
  (:require [taoensso.carmine :as car]
            [cheshire.core :as json]
            [clj-http.client :as http]))

(defmacro wcar* [& body] `(car/wcar {} ~@body))

(defn push-id
  "Takes a user id that's stored on the rethinkdb server
   and fetches the corresponding push notification id"
  [user-id]
  (wcar* (car/get user-id)))

(def gcm-key
  "Server key that allows us to use GCM"
  "key=AIzaSyBIqF5MZtmWS7cq8q2n5fYAnMrC0Ffnx-0")

(def gcm-url
  "The url to send it to"
  "https://gcm-http.googleapis.com/gcm/send")

(defn set-push-id
  "Set the push id."
  [{:keys [push-id user-id]}]
  (wcar* (car/set user-id push-id)))

(defn- make-req-message [payload]
  (str (:name (:from payload)) " kinda fancies you! You in to him or nah?"))

(defn- make-reply-message [payload]
  (str (:name (:from payload)) " said yes! Go get 'em!"))

(defn gcm-req-options [payload]
  {:headers {"Authorization" gcm-key}
   :content-type :json
   :body (json/generate-string
          {
           :registration_ids [(push-id (:to payload))]
           :data {:title "Someone's into you!"
                  :icon ""
                  :message (make-req-message payload)
                  :from-info (:from payload)}})})

(defn gcm-ok-options [payload]
  {:headers {"Authorization" gcm-key}
   :content-type :json
   :body (json/generate-string {
                                :registration_ids [(push-id (:to payload))]
                                :data {:title "Someone said yes!"
                                       :icon ""
                                       :from-info (:from payload)
                                       :message (make-reply-message payload)}})})

(defn send-req
  "Sends a friend request"
  [payload]
  (prn (http/post gcm-url (gcm-req-options payload))))

(defn send-okay
  "They're into you! Oooooh."
  [payload]
  (http/post gcm-url (gcm-ok-options payload)))
